## Prérequis

Afin de rendre vos travaux pratiques accessibles sur la plateforme, ils vous faut nous donner une archive (un dossier compressé) contenant les fichiers suivant :
 - les Notebooks du TP (.ipynb)
 - un fichier contenant la description du TP à afficher sur la plateforme (.pdf ou .txt, ...)
 - un dossier `binder` contenant un seul fichier `environment.yml` et un seul fichier `requirements.txt`
 - un dossier `data` contenant les données utilisées dans votre TP (.csv, ...)

### Création du fichier `environment.yml`

Le fichier `environment.yml` contient la liste des dépendences de l'environnement Conda sur lequel vous avez créé les Notebooks de votre TP. Ces dépendences ont été installées avec la commande `conda install`.

Cette liste de dépendences permet la création d'un environnement avec les mêmes paquets Python, R, C/C++ que ceux que vous avez utilisé.

Après avoir activé l'environnement Conda, il vous est possible de générer le fichier `environment.yml` avec la ligne de commande suivante :

```
conda env export --from-history -f environment.yml
```

Références :

https://repo2docker.readthedocs.io/en/latest/howto/export_environment.html#the-solution

https://conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#sharing-an-environment

### Création du fichier `requirements.txt`

Le fichier `requirements.txt` contient la liste des librairies Python utilisées dans les Notebooks de votre TP. Ces librairies ont été installées avec la commande `pip install`.

Cette liste de librairie permet d'installer toutes les librairies nécessaires lors de la création de l'environnement et évite aux étudiants d'avoir à les installer.

Si vous utilisez un environnement Python dédié à votre TP, il vous est possible de générer le fichier `requirements.txt` uniquement à partir des installations de l'environnement Python avec la ligne de commande suivante :

```
pip freeze > requirements.txt
```

Autrement, pour générer le fichier `requirements.txt` uniquement à partir des importations d'un Notebook de votre TP, il vous est possible d'utiliser une librairie Python tel que `pipreqsnb` :

```
pip install pipreqsnb
pipreqsnb ./
```

Références :

https://pip.pypa.io/en/stable/cli/pip_freeze/

https://github.com/ivanlen/pipreqsnb

## Mise en ligne

Le contenu de l'archive sera déposé sur son propre projet GitLab sur https://gricad-gitlab.univ-grenoble-alpes.fr/.

La plateforme se servira du lien git du projet de votre TP pour créer, une fois seulement (i.e. pas de mise-à-jour du TP), l'image de l'environnement de votre TP à partir de laquelle les environnements / containers seront créés.

Une fois la mise en ligne du TP sur la plateforme terminée, vous recevrez un code à donner aux étudiants qui participeront au TP. Ce code devra être entré par les étudiants sur la plateforme pour qu'ils puissent lancer le TP.
